<html>
<head>
   <link rel="stylesheet" type="text/css" href="theme/grey/styles.css">
 <style>
    tr:nth-child(odd)
     { 
	   background-color: grey
	  
	 }
    
     tr:nth-child(even)
     { 
	   background-color: yellow	  
	 }	
	 
	 thead th
	 {
	   background-color: white
	 }
	 
	 td 
	 {
	  font-style: normal
	 
	 }
	 
	 .salary-green {
		color : green;
	 }
	 
	 .salary-blue {
		 color : blue;
	 }
 </style>
</head>

  <body>
   <table border="1" >
   
     <thead>
    
       <th>Employee #</th>
	   <th>Name</th>
	   <th>Phone</th>
	   <th>Address</th>
	   <th>Salary</th>
     
	 </thead>
	 
	 <tbody>
	 <tr>
	   <td>0001</td>
	   <td>Ellen Bunton</td>
	   <td>847-494-4345</td>
	   <td style="font-style:italic">2426 Hassell Road, Hoffman estates, IL 60169</td>
	   <td class="salary">70,000 USD</td>
	 </tr>
	 
	  <tr>
	   <td>0002</td>
	   <td>Bob Whoolmer</td>
	   <td>847-494-3610</td>
	   <td>1222 Barrington Road, Hoffman estates, IL 60169</td>
	   <td class="salary">80,000 USD</td>
	 </tr>
	 
	  <tr>
	   <td>0003</td>
	   <td>Stacey Lacalamita</td>
	   <td>847-494-2532</td>
	   <td>342 Cutters Street, Schaunberg, IL 23832</td>
	   <td class="salary">75,000 USD</td>
	 </tr>
	 
	 <tr>
	   <td>0004</td>
	   <td>Cara Degraph</td>
	   <td>847-494-2383</td>
	   <td>1831 Main Street, Elgin, IL 23432</td>
	   <td class="salary">85,000 USD</td>
	   
	 </tr>
	 
	 <tr>
	   <td>0005</td>
	   <td>Mick Worthy</td>
	   <td>847-494-2353</td>
	   <td>3244 Golf Road, Chicago, IL 23233</td>
	   <td class="salary">80,000 USD</td>
	 </tr>
	 
	 <tr>
	   <td>0006</td>
	   <td>Saurabh Tiwari</td>
	   <td>847-494-2352</td>
	   <td>21 Kristien, Schaunberg, IL 24522</td>
	   <td class="salary">70,000 USD</td>
	 </tr>
	 </tbody>
	<table>
	
	
	<?php
		$records = array(1000, 15000, 20000, 5000, 50000);
	?>
	<table border="1">
		<?php foreach($records as $key => $val) { ?>
		<tr>
			<td style="color: <?php echo ($val > 10000) ? 'red' : 'blue'; ?>;">
				<?php echo $val; ?>
			</td>
		</tr>
		<?php } ?>
	</table>
	
	<h3>JS Records</h3>
	<div id="record_js">
	</div>
   </body>
   
   
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
   <script>
		
		$(document).ready(function jsFun() {
			
			var myrec = [
							{"name" : "Shailesh", "lastname" : "Sonare", "salary" : "20000"}, 
							{"name" : "Sunil", "lastname" : "Vachhani", "salary" : "50000"}
						];
			
			for(i = 0; i < myrec.length; i++) {
				
				
				var color_class = (myrec[i].salary > 20000) ? "salary-green" : "salary-blue";
				
				//$('#record_js').append("<div class='"+ color_class +"'>" + myrec[i].name + "  " + myrec[i].lastname + " " + myrec[i].salary +  "</div>");
				var rec_str = "<div class='"+ color_class +"'>";
				rec_str = rec_str + myrec[i].name + "  ";
				rec_str = rec_str + myrec[i].lastname + " ";
				rec_str = rec_str + myrec[i].salary +  "</div>";
				$('#record_js').append(rec_str);
			}
			
		});
   </script>
</html>